const axios = require('axios')
const { BigQuery } = require('@google-cloud/bigquery')
const bigQuery = new BigQuery();

const query = 'SELECT ImageURL FROM `corded-shard-229822.InternProject.dswdata_6_19_2` LIMIT 20';
let successCount = 0;

bigQuery.createQueryStream(query)
    .on('error', console.error)
    .on('data', processRow)
    .on('end', () => console.log('end'));

function processRow({ ImageURL }) {
    axios.post('https://us-central1-corded-shard-229822.cloudfunctions.net/do_everything', {
        ImageURL
    })
     .then(() => console.log(successCount++))
     .catch(() => {
        console.error('error');
        processRow({ ImageURL });
    });
}
//async functions
// when wud be returned promise, call await, Js will unwrap promise for you